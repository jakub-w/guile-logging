;; Copyrignt (C) 2021 Jakub Wojciech
;;
;; This file is part of Guile-loging.
;;
;; Guile-loging is free software: you can redistribute it and/or modify it
;; under the terms of the GNU General Public License as published by the
;; Free Software Foundation, either version 3 of the License, or (at your
;; option) any later version.
;;
;; Guile-loging is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
;; Public License for more details.
;;
;; You should have received a copy of the GNU General Public License along
;; with guile-logging. If not, see <https://www.gnu.org/licenses/>.

(library (logging logging)
  (export log-critical
	  log-error
	  log-warning
	  log-info
	  log-debug
	  log-trace
	  log-increase-verbosity
	  log-decrease-verbosity
	  log-set-verbosity
	  log-current-verbosity
	  log-critical-port
	  log-error-port
	  log-warning-port
	  log-info-port
	  log-debug-port
	  log-trace-port)
  (import (guile)
	  (rnrs io ports)
	  (ice-9 exceptions)))


(define log-levels #(trace debug info warning error critical))

(define LOG_LEVEL_TRACE 0)
(define LOG_LEVEL_DEBUG 1)
(define LOG_LEVEL_INFO 2)
(define LOG_LEVEL_WARNING 3)
(define LOG_LEVEL_ERROR 4)
(define LOG_LEVEL_CRITICAL 5)

(define verbosity LOG_LEVEL_INFO)

(define-syntax define-log-macro
  (lambda (x)
    (define (make-log-macro-name name)
      (datum->syntax
       name
       (string->symbol
	(string-append "log-" (symbol->string (syntax->datum name))))))

    (define (make-log-immediate-fun-name name)
      (datum->syntax
       name
       (string->symbol
	(string-append "log-"
		       (symbol->string (syntax->datum name))
		       "-immediate"))))

    (define (make-log-port-name name)
      (datum->syntax
       name
       (string->symbol
	(string-append
	 "log-" (symbol->string (syntax->datum name)) "-port"))))

    (define (make-log-prefix name)
      (string-append
       "[" (string-capitalize (symbol->string (syntax->datum name))) "] "))

    (syntax-case x ()
      ((k level severity default-port)
       (with-syntax ((log-macro (make-log-macro-name (syntax level)))
		     (log-immediate-fun (make-log-immediate-fun-name (syntax level)))
		     (log-port (make-log-port-name (syntax level)))
		     (pfx (make-log-prefix (syntax level))))
	 (syntax
	  (begin
	    (define log-port (make-parameter #f))

	    (define (log-immediate-fun fmt . args)
	      "This is an immediate logging function. It will process all the
arguments passed to it, without deferring.
It also doesn't check the current logging verbosity and logs unconditionally.

It's not really supposed to be used from Scheme code."
	      (let ((f (string-append pfx fmt "\n")))
		(apply format (or (log-port) default-port) f args)))

	    (define-syntax log-macro
	      (syntax-rules ()
		((_ fmt arg (... ...))
		 (when (<= verbosity severity)
		   (log-immediate-fun fmt arg (... ...)))))))))))))

(define-log-macro critical LOG_LEVEL_CRITICAL (current-error-port))
(define-log-macro error LOG_LEVEL_ERROR (current-error-port))
(define-log-macro warning LOG_LEVEL_WARNING (current-error-port))
(define-log-macro info LOG_LEVEL_INFO (current-output-port))
(define-log-macro debug LOG_LEVEL_DEBUG (current-output-port))
(define-log-macro trace LOG_LEVEL_TRACE (current-output-port))

(define (log-increase-verbosity)
  (set! verbosity (max LOG_LEVEL_TRACE (1- verbosity))))
(define (log-decrease-verbosity)
  (set! verbosity (min LOG_LEVEL_CRITICAL (1+ verbosity))))
(define (log-current-verbosity)
  (vector-ref log-levels verbosity))
(define (log-set-verbosity level)
  "Set logging verbosity to LEVEL.

LEVEL is on of these symbols: trace debug info warning error critical."
  (let ((last (1- (vector-length log-levels))))
    (let loop ((i 0))
      (cond
       ((> i last)
	(raise-exception (make-exception
			  (make-exception-with-origin 'log-set-verbosity)
			  (make-exception-with-message "Bad argument")
			  (make-exception-with-irritants level))))
       ((eq? level (vector-ref log-levels i))
	(set! verbosity i))
       (else (loop (1+ i)))))))
